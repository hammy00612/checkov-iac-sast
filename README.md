# Checkov IaC SAST CI CD Plugin Extension

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/-/ide/project/guided-explorations/ci-cd-plugin-extensions/checkov-iac-sast/)

## Not A Production Solution

The following disclaimer is emited to the logs by this plugin.  When using it in your own projects, you must copy the repository or it's files and then you can remove the code that emits this statement.

"The original location for this plugin extension is for demonstration and learning purposes only.  There is no breaking change SLA and it may change at any time without notice. If you need to depend on it for production or other critical systems, please make your own isolated copy to depend on.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-11-21

* **GitLab Version Released On**: 13.5

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/)(https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **References and Featured In**:
* [Checkov Documentation](https://www.checkov.io/documentation.html)
* [Checkov Official GitLab Integration Docs](https://www.checkov.io/4.Integrations/gitlab.html) - does not collect test results.

## Demonstrates These Design Requirements and Desirements

As a complete whole, this guided exploration requires at least the Free / Core (![FC](images/FC.png)) edition of GitLab.

- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** a file templating engine that substitutes GitLab CI variables into a file where special template insertion markup is used.
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** a plugin extension that consists of an includable GitLab CI yml job that is standardized and reusable.
* **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Consume testing tools outputted junit XML to display test results.

#### GitLab CI Functionality:

- **.gitlab-ci.yml** "includes" [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/)can be a file in the same repo, another repo or an https url
  - Used to enable manage main build templates (gitlab ci custom functions) to have more change controls over them.
- **.gitlab-ci.yml** "extends" [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) parameter for "template jobs" (gitlab ci custom functions).
- **.gitlab-ci.yml** "allow_failure: true" [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) enables all security scanners to run.
- **.gitlab-ci.yml** "allow_failure: true" [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) Compatible with but not dependent on GitLab AutoDevOps

## Using This Pattern:
- This repository contains a GitLab CI file showing the use of the plugin: [calling-plugin-sample.yml](./calling-plugin-sample.yml)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## CI CD Plugin Extension 

This guided exploration implements CI CD library code in the form of a plugin extension.  It also models the general concept of building a standardized, version controlled plugin extension in GitLab.
